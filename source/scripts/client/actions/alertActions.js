var AlertActions = Reflux.createActions(['getAlerts', 'addAlert', 'delAlert']);

module.exports = AlertActions;
