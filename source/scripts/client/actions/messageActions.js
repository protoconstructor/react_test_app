var MessageActions = Reflux.createActions(['getMessages', 'loadMessages', 'addMessage', 'complete']);

module.exports = MessageActions;
