require('../../../css/main.styl'); //styles

window.Request = require('../common-modules/serverInteraction');
import { Router, Route, IndexRoute } from 'react-router';
let ReactDOM = require('react-dom');
let Layout = require('./layout.jsx');
let NavBar = require('./navbar.jsx');
let About = require('./about.jsx');

let App = React.createClass({
  render() {
    return (
      <div className="container-fluid">
        <NavBar />
        <div id="childrens">
          {this.props.children}
        </div>
      </div>
    )
  }
});

ReactDOM.render(
  (<Router>
    <Route path="/" component={App}>
      <IndexRoute component={Layout} />
      <Route path="about" component={About}/>
    </Route>
  </Router>),
  document.getElementById('content')
);
