var mongoose = require('mongoose');
var logger = require('../common/logger')(module);
var config = require('../server/config');

mongoose.connect(config.get('dbhost:uri'));

var db = mongoose.connection;

db.on('error', (err) => logger.error('connection error:', err.message));
db.once('open', () => logger.info("Connected to DB!"));

var Schema = mongoose.Schema;

// Schemas
var Message = new Schema({
    text: { type: String, required: true },
    user: { type: String, required: true },
    date: { type: Date, default: Date.now }
});

// validation
Message.path('text').validate(function (v) {
    return v.length > 5 && v.length < 1000;
});

var MessageModel = mongoose.model('Message', Message);

module.exports.MessageModel = MessageModel;
