"use strict"

var express = require('express');
var router = express.Router();
var MessageModel = require('../server/data-context').MessageModel;
var logger = require('../common/logger')(module);

/*api:start*/
router.get('/', function(req, res) {
  return MessageModel.find(function (err, messages) {
    if (!err) {
      logger.info("load messages");
      return res.send(messages);
    } else {
      res.statusCode = 500;
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
});

router.post('/', function(req, res) {
  var newMessage = new MessageModel({
    text: req.body.text,
    user: req.body.user
  });

  newMessage.save(function (err) {
    if (!err) {
        logger.info("message created");
        return res.send({ status: 'OK', message: newMessage });
    } else {
        if(err.name == 'ValidationError') {
            res.statusCode = 400;
            res.send({ error: 'Validation error' });
        } else {
            res.statusCode = 500;
            res.send({ error: 'Server error' });
        }
        logger.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
});

router.get('/:id', function(req, res) {
  return MessageModel.findById(req.params.id, function (err, message) {
    if(!message) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    if (!err) {
      return res.send({ status: 'OK', message: message });
    } else {
      res.statusCode = 500;
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.send({ error: 'Server error' });
    }
  });
});

router.put('/:id', function (req, res){
  return MessageModel.findById(req.params.id, function (err, message) {
    if(!message) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }

    message.text = req.body.text;
    message.user = req.body.user;

    return message.save(function (err) {
      if (!err) {
        logger.info("message updated");
        return res.send({ status: 'OK', message: message });
      } else {
        if(err.name == 'ValidationError') {
          res.statusCode = 400;
          res.send({ error: 'Validation error' });
        } else {
          res.statusCode = 500;
          res.send({ error: 'Server error' });
        }

        logger.error('Internal error(%d): %s', res.statusCode, err.message);
      }
    });
  });
});

router.delete('/:id', function (req, res){
  return MessageModel.findById(req.params.id, function (err, message) {
    if(!message) {
      res.statusCode = 404;
      return res.send({ error: 'Not found' });
    }
    return message.remove(function (err) {
      if (!err) {
        logger.info("message removed");
        return res.send({ status: 'OK' });
      } else {
        res.statusCode = 500;
        logger.error('Internal error(%d): %s', res.statusCode, err.message);
        return res.send({ error: 'Server error' });
      }
    });
  });
});
/*api:end*/

module.exports = router;
